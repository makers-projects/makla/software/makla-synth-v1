/*
 * waves.c
 *
 *  Created on: Jun 3, 2023
 *      Author: jojo
 */

#ifndef INC_WAVES_C_
#define INC_WAVES_C_

#include <stdint.h>

extern const uint16_t wave_sine[];
extern const uint16_t wave_triangle[];
extern const uint16_t wave_sliced1[];
extern const uint16_t wave_sliced2[];
extern const uint16_t wave_added1[];
extern const uint16_t wave_added2[];
extern const uint16_t wave_added3[];
extern const uint16_t wave_added4[];

#endif /* INC_WAVES_C_ */
